import React, {useState} from 'react'
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import SearchBar from '../../components/SearchBar';
import yelp from '../api/yelp';
import useResults from '../hooks/useResults';
import Results from '../../components/ResultsList';
import ResultsList from '../../components/ResultsList';

const SearchScreen = () => {

    const [term, setTerm] = useState('');
    const [searchApi, results, errorMessage] = useResults();

    console.log(results);

    const filterResultsByPrice = (price) => {
        // price === '$' || === '$$' etc
        return results.filter(result => { // for every result inside our array
            return result.price === price; // if this is true return
        })




    }
   
    return (
        <>
            <SearchBar term={term} 
            onTermChange={setTerm} // newTerm) => setTerm(newTerm)
            onTermSubmit ={() => searchApi(term)} //() => searchApi
            /> 
    <Text>{errorMessage ? <Text> {errorMessage} </Text> :null}</Text>
    <Text>We have found {results.length} results</Text>
    <ScrollView>

    <ResultsList results={filterResultsByPrice('$')}title="Cost Effective"/>
    <ResultsList results={filterResultsByPrice('$$')}title="Bit Pricier" />
    <ResultsList results={filterResultsByPrice('$$$')}title="Big Spender"/>
    </ScrollView>
        </>



    )




}

export default SearchScreen;