import { useState, useEffect } from "react";
import yelp from '../api/yelp';

export default () => {
    const [results, setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    // (newTerm) paremeter passed in, adding to setTerm


 
    const searchApi = async (searchTerm) => {
        try{
        const response = await yelp.get('/search', {
            params: {
                limit: 50,
                searchTerm,
                location: 'san jose'

            }

        });
        setResults(response.data.businesses)
    }
    catch (err) {
        console.log(err)
        setErrorMessage('Something went wrong')


    }
    };

    useEffect(() => {
        searchApi('pasta');
    }, [])

    return [searchApi, results, errorMessage];



};